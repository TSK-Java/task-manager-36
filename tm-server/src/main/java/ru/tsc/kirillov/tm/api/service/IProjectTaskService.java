package ru.tsc.kirillov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.model.Project;

public interface IProjectTaskService {

    void bindTaskToProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    void unbindTaskToProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    @Nullable
    Project removeProjectById(@Nullable String userId, @Nullable String projectId);

    @Nullable
    Project removeProjectByIndex(@Nullable String userId, @Nullable Integer index);

    void clear(@Nullable String userId);

}
