package ru.tsc.kirillov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import ru.tsc.kirillov.tm.api.repository.IProjectRepository;
import ru.tsc.kirillov.tm.api.repository.ITaskRepository;
import ru.tsc.kirillov.tm.enumerated.Sort;
import ru.tsc.kirillov.tm.model.Project;
import ru.tsc.kirillov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public final class TaskRepositoryTest {

    @NotNull final ITaskRepository repository = new TaskRepository();

    @NotNull final String userId = UUID.randomUUID().toString();

    @NotNull final String taskName = UUID.randomUUID().toString();

    @Test
    public void add() {
        Assert.assertEquals(0, repository.count());
        @NotNull final Task task = new Task();
        task.setName(taskName);
        task.setUserId(userId);
        repository.add(task);
        Assert.assertEquals(1, repository.count());
        @Nullable Task taskFind = repository.findOneById(task.getId());
        Assert.assertNotNull(taskFind);
        Assert.assertEquals(task, taskFind);
        Assert.assertEquals(task.getName(),taskFind.getName());
    }

    @Test
    public void addByUserId() {
        Assert.assertEquals(0, repository.count());
        @NotNull final Task task = new Task();
        task.setName(taskName);
        task.setUserId(userId);
        repository.add(userId, task);
        Assert.assertEquals(1, repository.count());
        Assert.assertEquals(1, repository.count(userId));
        Assert.assertEquals(0, repository.count(UUID.randomUUID().toString()));
        @Nullable Task taskFind = repository.findOneById(task.getId());
        Assert.assertNotNull(taskFind);
        Assert.assertEquals(task, taskFind);
        Assert.assertEquals(task.getName(), taskFind.getName());
    }

    @Test
    public void addList() {
        Assert.assertEquals(0, repository.count());
        @NotNull List<Task> tasks = new ArrayList<>();
        final int countTask = 10;
        for (int i = 0; i < countTask; i++) {
            @NotNull final String userId = UUID.randomUUID().toString();
            @NotNull final String taskName = UUID.randomUUID().toString();
            @NotNull final Task task = new Task();
            task.setName(taskName);
            task.setUserId(userId);
            tasks.add(task);
        }
        repository.add(tasks);
        Assert.assertEquals(countTask, repository.count());
        for (int i = 0; i < countTask; i++) {
            @NotNull final Task task = tasks.get(i);
            Assert.assertNull(repository.findOneById(UUID.randomUUID().toString(), task.getId()));
            @Nullable final Task taskFind = repository.findOneById(task.getId());
            Assert.assertNotNull(taskFind);
            Assert.assertEquals(tasks.get(i).getName(), taskFind.getName());
        }
    }

    @Test
    public void create() {
        Assert.assertEquals(0, repository.count());
        @NotNull final Task task = repository.create(userId, taskName);
        Assert.assertEquals(1, repository.count());
        Assert.assertEquals(1, repository.count(userId));
        Assert.assertEquals(0, repository.count(UUID.randomUUID().toString()));
        Assert.assertNull(repository.findOneById(UUID.randomUUID().toString(), task.getId()));
        @Nullable final Task taskFind = repository.findOneById(userId, task.getId());
        Assert.assertNotNull(taskFind);
        Assert.assertEquals(task, taskFind);
        Assert.assertEquals(task.getName(), taskFind.getName());
    }

    @Test
    public void createDescription() {
        Assert.assertEquals(0, repository.count());
        @NotNull final String taskDescription = UUID.randomUUID().toString();
        @NotNull final Task task = repository.create(userId, taskName, taskDescription);
        Assert.assertEquals(1, repository.count());
        Assert.assertEquals(1, repository.count(userId));
        Assert.assertEquals(0, repository.count(UUID.randomUUID().toString()));
        Assert.assertNull(repository.findOneById(UUID.randomUUID().toString(), task.getId()));
        @Nullable final Task taskFind = repository.findOneById(userId, task.getId());
        Assert.assertNotNull(taskFind);
        Assert.assertEquals(task, taskFind);
        Assert.assertEquals(task.getDescription(), taskFind.getDescription());
    }

    @Test
    public void clear() {
        Assert.assertEquals(0, repository.count());
        repository.create(userId, taskName);
        Assert.assertEquals(1, repository.count());
        Assert.assertEquals(1, repository.count(userId));
        Assert.assertEquals(0, repository.count(UUID.randomUUID().toString()));
        repository.clear();
        Assert.assertEquals(0, repository.count());
        Assert.assertEquals(0, repository.count(userId));
        Assert.assertEquals(0, repository.count(UUID.randomUUID().toString()));
    }

    @Test
    public void findAll() {
        repository.create(userId, taskName);
        @Nullable List<Task> tasks = repository.findAll();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(1, tasks.size());
    }

    @Test
    public void findAllUserId() {
        repository.create(userId, taskName);
        @Nullable final String userNullId = null;
        Assert.assertEquals(0, repository.findAll(userNullId).size());
        Assert.assertEquals(0, repository.findAll(UUID.randomUUID().toString()).size());
        Assert.assertEquals(1, repository.findAll(userId).size());
    }

    @Test
    public void findAllComparatorNullUserId() {
        repository.create(userId, taskName);
        @Nullable final String userNullId = null;
        Assert.assertEquals(0, repository.findAll(userNullId, Sort.BY_NAME.getComparator()).size());
    }

    @Test
    public void findAllComparator() {
        final int countTask = 10;
        for (int i = 0; i < countTask; i++) {
            @NotNull final String taskName = String.format("Task_%d", countTask - i - 1);
            repository.create(userId, taskName);
        }
        @NotNull List<Task> tasks = repository.findAll(userId, Sort.BY_NAME.getComparator());
        Assert.assertEquals(countTask, tasks.size());
        for (int i = 0; i < countTask; i++) {
            @NotNull final String taskName = String.format("Task_%d", i);
            Assert.assertEquals(taskName, tasks.get(i).getName());
        }
    }

    @Test
    public void existsById() {
        Assert.assertEquals(0, repository.count());
        @NotNull final Task task = repository.create(userId, taskName);
        Assert.assertTrue(repository.existsById(task.getId()));
        Assert.assertFalse(repository.existsById(UUID.randomUUID().toString()));
        Assert.assertFalse(repository.existsById(null, task.getId()));
        Assert.assertFalse(repository.existsById("", task.getId()));
        Assert.assertFalse(repository.existsById(UUID.randomUUID().toString(), task.getId()));
        Assert.assertTrue(repository.existsById(userId, task.getId()));
        Assert.assertFalse(repository.existsById(userId, UUID.randomUUID().toString()));
        Assert.assertFalse(repository.existsById(userId, ""));
        Assert.assertFalse(repository.existsById(userId, null));
    }

    @Test
    public void findOneById() {
        Assert.assertEquals(0, repository.count());
        @NotNull final Task task = repository.create(userId, taskName);
        Assert.assertNotNull(repository.findOneById(task.getId()));
        Assert.assertNull(repository.findOneById(UUID.randomUUID().toString()));
        Assert.assertNull(repository.findOneById(null, task.getId()));
        Assert.assertNull(repository.findOneById("", task.getId()));
        Assert.assertNull(repository.findOneById(UUID.randomUUID().toString(), task.getId()));
        Assert.assertNotNull(repository.findOneById(userId, task.getId()));
        Assert.assertNull(repository.findOneById(userId, UUID.randomUUID().toString()));
        Assert.assertNull(repository.findOneById(userId, ""));
        Assert.assertNull(repository.findOneById(userId, null));
        @Nullable final Task taskFind = repository.findOneById(userId, task.getId());
        Assert.assertNotNull(taskFind);
        Assert.assertEquals(task.getId(), taskFind.getId());
    }

    @Test
    public void findOneByIndex() {
        Assert.assertEquals(0, repository.count());
        @NotNull final Task task = repository.create(userId, taskName);
        Assert.assertNotNull(repository.findOneByIndex(0));
        Assert.assertEquals(task.getId(), repository.findOneByIndex(0).getId());
        Assert.assertNotNull(repository.findOneByIndex(userId, 0));
        Assert.assertNull(repository.findOneByIndex(null, 0));
        Assert.assertNull(repository.findOneByIndex("", 0));
        Assert.assertNull(repository.findOneByIndex(UUID.randomUUID().toString(), 0));
    }

    @Test
    public void remove() {
        Assert.assertEquals(0, repository.count());
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final String taskName = UUID.randomUUID().toString();
        @NotNull Task task = repository.create(userId, taskName);
        @Nullable Task taskRemove = repository.remove(task);
        Assert.assertEquals(0, repository.count());
        Assert.assertEquals(task, taskRemove);
        task = repository.create(userId, taskName);
        repository.create(UUID.randomUUID().toString(), taskName);
        Assert.assertEquals(2, repository.count());
        taskRemove = repository.remove(null, null);
        Assert.assertNull(taskRemove);
        Assert.assertEquals(2, repository.count());
        taskRemove = repository.remove(null, task);
        Assert.assertNull(taskRemove);
        Assert.assertEquals(2, repository.count());
        taskRemove = repository.remove(userId, null);
        Assert.assertNull(taskRemove);
        Assert.assertEquals(2, repository.count());
        taskRemove = repository.remove(UUID.randomUUID().toString(), task);
        Assert.assertNull(taskRemove);
        Assert.assertEquals(2, repository.count());
        taskRemove = repository.remove(userId, task);
        Assert.assertEquals(task, taskRemove);
        Assert.assertEquals(1, repository.count());
    }

    @Test
    public void removeAll() {
        Assert.assertEquals(0, repository.count());
        @NotNull List<Task> tasks = new ArrayList<>();
        final int countTask = 10;
        for (int i = 0; i < countTask; i++) {
            @NotNull final String userId = UUID.randomUUID().toString();
            @NotNull final String userName = UUID.randomUUID().toString();
            @NotNull final Task task = new Task();
            task.setName(userName);
            task.setUserId(userId);
            tasks.add(task);
        }
        repository.add(tasks);
        Assert.assertEquals(countTask, repository.count());
        repository.removeAll(null);
        Assert.assertEquals(countTask, repository.count());
        tasks.remove(0);
        repository.removeAll(tasks);
        Assert.assertEquals(1, repository.count());
    }

    @Test
    public void removeById() {
        Assert.assertEquals(0, repository.count());
        @NotNull Task task = repository.create(userId, taskName);
        @Nullable Task taskRemove = repository.removeById(UUID.randomUUID().toString());
        Assert.assertNull(taskRemove);
        Assert.assertEquals(1, repository.count());
        taskRemove = repository.removeById(task.getId());
        Assert.assertEquals(0, repository.count());
        Assert.assertEquals(task, taskRemove);

        task = repository.create(userId, taskName);
        repository.create(UUID.randomUUID().toString(), taskName);
        Assert.assertEquals(2, repository.count());
        taskRemove = repository.removeById(UUID.randomUUID().toString(), task.getId());
        Assert.assertNull(taskRemove);
        Assert.assertEquals(2, repository.count());
        taskRemove = repository.removeById(UUID.randomUUID().toString(), UUID.randomUUID().toString());
        Assert.assertNull(taskRemove);
        Assert.assertEquals(2, repository.count());
        taskRemove = repository.removeById(userId, UUID.randomUUID().toString());
        Assert.assertNull(taskRemove);
        Assert.assertEquals(2, repository.count());
        taskRemove = repository.removeById(userId, task.getId());
        Assert.assertEquals(task, taskRemove);
        Assert.assertEquals(1, repository.count());
    }

    @Test
    public void removeByIndex() {
        Assert.assertEquals(0, repository.count());
        @NotNull Task task = repository.create(userId, taskName);
        @Nullable Task taskRemove = repository.removeByIndex(0);
        Assert.assertNotNull(taskRemove);
        Assert.assertEquals(0, repository.count());
        Assert.assertEquals(task, taskRemove);
    }

    @Test
    public void count() {
        Assert.assertEquals(0, repository.count());
        final int countTask = 10;
        for (int i = 0; i < countTask; i++) {
            @NotNull final String userId = UUID.randomUUID().toString();
            @NotNull final String taskName = UUID.randomUUID().toString();
            repository.create(userId, taskName);
            Assert.assertEquals(i + 1, repository.count());
            Assert.assertEquals(repository.count(), repository.findAll().size());
        }
    }

    @Test
    public void findAllByProjectId() {
        @NotNull final IProjectRepository projectRepository = new ProjectRepository();
        @NotNull final ITaskRepository taskRepository = new TaskRepository();
        @NotNull final String projectName = UUID.randomUUID().toString();
        @NotNull final Project project = projectRepository.create(userId, projectName);
        @NotNull final Task task = taskRepository.create(userId, taskName);
        @NotNull List<Task> tasks =  taskRepository.findAllByProjectId(userId, project.getId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(0, tasks.size());
        task.setProjectId(project.getId());
        tasks =  taskRepository.findAllByProjectId(userId, project.getId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(1, tasks.size());
        tasks =  taskRepository.findAllByProjectId(null, project.getId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(0, tasks.size());
        tasks =  taskRepository.findAllByProjectId(null, null);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(0, tasks.size());
        tasks =  taskRepository.findAllByProjectId(userId, null);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(0, tasks.size());
    }

    @Test
    public void removeAllByProjectId() {
        @NotNull final IProjectRepository projectRepository = new ProjectRepository();
        @NotNull final ITaskRepository taskRepository = new TaskRepository();
        @NotNull final String projectName = UUID.randomUUID().toString();
        @NotNull final Project project = projectRepository.create(userId, projectName);
        @NotNull final Project project_1 = projectRepository.create(userId, projectName);
        @NotNull Task task = taskRepository.create(userId, taskName);
        task.setProjectId(project.getId());
        taskRepository.create(userId, projectName);
        task = taskRepository.create(UUID.randomUUID().toString(), taskName);
        task.setProjectId(project.getId());
        task = taskRepository.create(UUID.randomUUID().toString(), taskName);
        task.setProjectId(project_1.getId());

        Assert.assertEquals(4, taskRepository.count());
        taskRepository.removeAllByProjectId(null, null);
        Assert.assertEquals(4, taskRepository.count());
        taskRepository.removeAllByProjectId(userId, null);
        Assert.assertEquals(4, taskRepository.count());
        taskRepository.removeAllByProjectId(null, project.getId());
        Assert.assertEquals(4, taskRepository.count());
        taskRepository.removeAllByProjectId(userId, project.getId());
        Assert.assertEquals(3, taskRepository.count());
    }

    @Test
    public void removeAllByProjectList() {
        @NotNull final IProjectRepository projectRepository = new ProjectRepository();
        @NotNull final ITaskRepository taskRepository = new TaskRepository();
        @NotNull final String projectName = UUID.randomUUID().toString();
        @NotNull final Project project = projectRepository.create(userId, projectName);
        @NotNull final Project project_1 = projectRepository.create(userId, projectName);
        int count = 5;
        for (int i = 0; i < count; i++) {
            @NotNull Task task = taskRepository.create(userId, UUID.randomUUID().toString());
            task.setProjectId(project.getId());
        }
        for (int i = 0; i < count; i++) {
            @NotNull Task task = taskRepository.create(userId, UUID.randomUUID().toString());
            task.setProjectId(project_1.getId());
        }
        for (int i = 0; i < count; i++) {
            taskRepository.create(userId, UUID.randomUUID().toString());
        }
        for (int i = 0; i < count; i++) {
            @NotNull Task task = taskRepository.create(userId, UUID.randomUUID().toString());
            task.setProjectId(UUID.randomUUID().toString());
        }
        Assert.assertEquals(20, taskRepository.count());
        taskRepository.removeAllByProjectList(
                UUID.randomUUID().toString(),
                new String[]{UUID.randomUUID().toString(), UUID.randomUUID().toString()});
        Assert.assertEquals(20, taskRepository.count());
        taskRepository.removeAllByProjectList(
                UUID.randomUUID().toString(),
                new String[]{project.getId(), project_1.getId()});
        Assert.assertEquals(20, taskRepository.count());
        taskRepository.removeAllByProjectList(
                userId,
                new String[]{project.getId(), project_1.getId()});
        Assert.assertEquals(10, taskRepository.count());
    }

}
